#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATION_H

#include "image.h"

enum ROTATION_DIRECTION {
    COUNTERCLOCKWISE = 10
};

enum ROTATION_DEGREE {
    DEGREE_90 = 0
};

struct image rotate(struct image source,
                    enum ROTATION_DIRECTION rot_dir,
                    enum ROTATION_DEGREE rot_degree);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATION_H
