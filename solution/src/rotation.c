#include "image.h"
#include "rotation.h"

#include <stdio.h>


typedef struct image (*rotate_handler)( struct image const );

static
struct image rotate_counterclockwise_degree90( struct image const source) {
    struct image rotated;
    create_image(&rotated, source.height, source.width);

    uint64_t h = source.height;
    uint64_t w = source.width;
    size_t i_rotated;
    for (size_t i = 0; i < source.width * source.height; i++) {
        i_rotated = h * (w - 1 - i % w) + i / w;
        rotated.data[i_rotated] = source.data[i];
    }

    return rotated;
}

static
rotate_handler handlers[] = {
        [COUNTERCLOCKWISE + DEGREE_90] = rotate_counterclockwise_degree90,
};

struct image rotate( struct image const source,
                     const enum ROTATION_DIRECTION rot_dir,
                     const enum ROTATION_DEGREE rot_degree ) {
    rotate_handler handler = handlers[rot_dir + rot_degree];
    return handler(source);
}
