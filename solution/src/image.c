#include "image.h"

#include <malloc.h>
#include <stdint.h>


void create_image(struct image* img, uint64_t width, uint64_t height) {
    img->width = width;
    img->height = height;
    img->data = (struct pixel*) malloc(sizeof (struct pixel) * width * height);
}
