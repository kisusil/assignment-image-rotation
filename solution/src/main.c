#include "image.h"
#include "image_bmp.h"
#include "rotation.h"

#include <malloc.h>
#include <stdio.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
        fprintf(stderr, "Wrong number of arguments. You have to pass 3");
        return 1;
    }

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");

    if (!in) {
        fprintf(stderr, "Error while open %s", argv[1]);
        return 1;
    }

    if (!out) {
        fprintf(stderr, "Error while open %s", argv[2]);
        return 1;
    }

    struct image img;

    enum read_status read_status = from_bmp(in, &img);
    if (read_status != READ_OK) {
        fclose(in);
        fclose(out);
        fprintf(stderr, "Error while reading: %d\n", read_status);
        return 1;
    }

    struct image img_rotated = rotate(img, COUNTERCLOCKWISE, DEGREE_90);

    if (to_bmp(out, &img_rotated) != WRITE_OK) {
        fclose(in);
        fclose(out);
        free(img.data);
        free(img_rotated.data);
        fprintf(stderr, "Error while writing\n");
        return 1;
    }

    free(img.data);
    free(img_rotated.data);
    fclose(in);
    fclose(out);
    return 0;
}
