#include "image_bmp.h"

#include "image.h"

#include <inttypes.h>
#include <malloc.h>
#include <stdbool.h>


#define BM_TYPE 19778
#define BIT_COUNT 24
#define NO_COMPRESSION 0

struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};

static bool read_header(FILE* file, struct bmp_header* header ) {
    return fread(header, sizeof( struct bmp_header ), 1, file );
}

static bool skip_offset(FILE* file, size_t bytes_amount) {
    uint8_t* junk = (uint8_t*) malloc(sizeof (uint8_t) * bytes_amount);
    bool result = fread(junk, sizeof(uint8_t), bytes_amount, file);
    free(junk);
    return result;
}

static uint64_t closest_mod_4(uint64_t width_bytes) {
    if (width_bytes % 4 == 0) {
        return width_bytes;
    }

    return width_bytes + (4 - width_bytes % 4);
}

static size_t byte_reverse_pos(size_t buffer_pos, struct image const* img) {
    size_t i = buffer_pos;
    uint64_t h = img->height;
    uint64_t w = img->width;
    // width of file line
    size_t x = closest_mod_4(w * 3);
    return i % x + (h - 1 - i / x) * x;
}

static size_t pixel_pos(size_t buffer_pos, struct image const* img) {
    size_t i = buffer_pos;
    uint64_t w = img->width;
    // width of file line
    size_t x = closest_mod_4(3 * w);
    // reversed byte position
    size_t k = byte_reverse_pos(i, img);
    return k / x * w + (k % x) / 3;
}

static uint64_t count_size(struct bmp_header const* header) {
    return closest_mod_4(header->biWidth * 3) * header->biHeight + 2;
}

static bool is_junk(size_t buffer_pos, struct image const* img) {
    size_t w = img->width;
    // reversed byte position
    size_t k = byte_reverse_pos(buffer_pos, img);
    size_t x = closest_mod_4(w * 3);
    return k % x >= 3 * w;
}

static bool read_bmp_image(FILE* file, struct bmp_header const* header, struct image* img) {
    size_t size = count_size(header);
    uint8_t* buffer = (uint8_t*) malloc(sizeof (uint8_t) * size);
    bool result = fread(buffer, sizeof (uint8_t), size, file);

    if (!result) {
        free(buffer);
        return false;
    }

    create_image(img, header->biWidth, header->biHeight);

    uint8_t pixel_buffer[3];
    size_t pb_i = 0;
    for (size_t i = 0; i < size - 2; i++) {
        if (is_junk(i, img)) {
            continue;
        }

        pixel_buffer[pb_i] = buffer[i];
        pb_i++;

        if (pb_i >= 3) {
            img->data[pixel_pos(i, img)] = (struct pixel) {
                .b = pixel_buffer[0],
                .g = pixel_buffer[1],
                .r = pixel_buffer[2]
            };
            pb_i = 0;
        }
    }

    free(buffer);
    return true;
}

static bool check_compression(struct bmp_header const* header) {
    return header->biCompression == NO_COMPRESSION;
}

static bool check_bit_count(struct bmp_header const* header) {
    return header->biBitCount == BIT_COUNT;
}

static bool check_bf_type(struct bmp_header const* header) {
    return header->bfType == BM_TYPE;
}

static bool check_header(struct bmp_header const* header) {
    return check_compression(header) && check_bit_count(header) && check_bf_type(header);
}

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header* header = (struct bmp_header*) malloc(sizeof (struct bmp_header));

    if (!read_header(in, header)) {
        free(header);
        return READ_INVALID_HEADER;
    }

    if (!check_header(header)) {
        free(header);
        return READ_INVALID_HEADER;
    }

    if (header->bOffBits > sizeof (struct bmp_header)) {
        if (!skip_offset(in, header->bOffBits - sizeof (struct bmp_header))) {
            free(header);
            return READ_INVALID_UNEXPECTED_ERROR;
        }
    }

    if (!read_bmp_image(in, header, img)) {
        free(header);
        return READ_INVALID_BODY;
    }

    free(header);
    return READ_OK;
}

static struct bmp_header* create_header(struct image const* img) {
    struct bmp_header* header = (struct bmp_header*) malloc(sizeof (struct bmp_header));
    header->bfType = BM_TYPE;
    header->biBitCount = BIT_COUNT;
    header->bOffBits = (uint32_t) sizeof (struct bmp_header);
    header->biCompression = NO_COMPRESSION;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biSize = 40;
    header->biPlanes = 1;
    header->biSizeImage = closest_mod_4(img->width * 3) * img->height + 2;
    header->bfileSize = header->bOffBits + header->biSizeImage;
    header->bfReserved = 0;
    header->biXPelsPerMeter = 0;
    header->biYPelsPerMeter = 0;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    return header;
}

static uint8_t get_component(struct image const* img, size_t pixel_i, size_t buffer_pos) {
    size_t junk_per_line = closest_mod_4(img->width * 3) - img->width * 3;
    size_t pos_without_junk = buffer_pos - buffer_pos / closest_mod_4(img->width * 3) * junk_per_line;
    if (pos_without_junk % 3 == 0) {
        return img->data[pixel_i].b;
    }

    if (pos_without_junk % 3 == 1) {
        return img->data[pixel_i].g;
    }

    return img->data[pixel_i].r;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header* header = create_header(img);
    uint8_t* buffer = (uint8_t*) malloc(sizeof (uint8_t) * header->biSizeImage);

    const uint64_t line_file_size = closest_mod_4(img->width * 3);
    const uint64_t line_img_size = img->width * 3;
    // reversed byte position
    size_t k =  0;
    for (size_t i = 0; i < header->biSizeImage - 2; i++) {
        k = byte_reverse_pos(i, img);
        if (k % line_file_size >= line_img_size) {
            buffer[i] = 0;
            continue;
        }

        buffer[i] = get_component(img, pixel_pos(i, img), i);
    }
    buffer[header->biSizeImage - 2] = 0;
    buffer[header->biSizeImage - 1] = 0;

    if (!fwrite(header, sizeof (struct bmp_header), 1, out)) {
        free(buffer);
        free(header);
        return WRITE_ERROR;
    }

    if (!fwrite(buffer, sizeof (uint8_t), header->biSizeImage, out)) {
        free(header);
        free(buffer);
        return WRITE_ERROR;
    }

    free(buffer);
    free(header);
    return WRITE_OK;
}
